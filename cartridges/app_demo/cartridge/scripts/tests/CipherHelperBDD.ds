var Assert = require('/app_test/cartridge/scripts/lib/Assert');
var bdd = require('/app_test/cartridge/scripts/lib/bdd');
var describe = bdd.describe;
var it = bdd.it;
var before = bdd.before;
var after = bdd.after;
var beforeEach = bdd.beforeEach;
var afterEach = bdd.afterEach;

var Logger = require('dw/system/Logger'); 

module.exports = 
describe('CipherHelper', function() {
	var CipherHelper = require('app_demo/cartridge/scripts/CipherHelper'),
		expectedCypherText = 'wFKBVVb/9+Xmk2XiR9sJiw==';
	
	describe('encrypt', function() {
		before(function () {
			Logger.debug('before-encrypt called');
		});
		
		after(function () {
			Logger.debug('after-encrypt called');
		});
		
		beforeEach(function () {
			Logger.debug('beforeEach-encrypt called');
		});
		
		afterEach(function () {
			Logger.debug('afterEach-encrypt called');
		});
		
		it.skip('should succeed when using valid input', function() {
			//Arrange
			let stringToEncrpt = 'testing',
				actual = undefined,
				expected = expectedCypherText;
				
			//Act
			actual = CipherHelper.encrypt(stringToEncrpt);
										
			//Assert
			Assert.areEqual(expected, actual);
		});
		
		it('should return null when input is null', function() {
			//Arrnge
			let stringToDecrypt = null,
				expected = null,
				actual = undefined;
			
			//Act
			actual = CipherHelper.encrypt(stringToDecrypt);	
			
			//Assert
			Assert.areEqual(expected, actual);
		});
		
		it('should return empty string when input is empty string', function() {
			//Arrange
			let stringToDecrypt = '',
				expected = '',
				actual = undefined;
			
			//Act	
			actual = CipherHelper.encrypt(stringToDecrypt);
			
			//Assert
			Assert.areEqual(expected, actual);
		});
		
		it('should succeed with 10kb input string executed 100 times', function() {
			//Arrange
			var SecureRandom = require('dw/crypto').SecureRandom;
			
			//Act
			for (let i = 0; i < 100; i++) {
				let secureRandom = new SecureRandom(),
					stringToEncrypt = StringUtils.encodeBase64(secureRandom.nextBytes(10240).toString()); //10kb = 10240 bytes
				CipherHelper.encrypt(stringToEncrypt);
			};
			
			//Assert		
		})
	});

	describe('decrypt', function() {
		before(function () {
			Logger.debug('before-decrypt called');
		});
		
		after(function () {
			Logger.debug('after-decrypt called');
		});
		
		beforeEach(function () {
			Logger.debug('beforeEach-decrypt called');
		});
		
		afterEach(function () {
			Logger.debug('afterEach-decrypt called');
		});
		
		it('should succeed when using valid input', function() {
			//Arrange
			let stringToDecrypt = expectedCypherText,
				actual = undefined,
				expected = 'testing';
			
			//Act
			actual = CipherHelper.decrypt(stringToDecrypt);
				
			//Assert
			Assert.areEqual(expected, actual);	
		});
	
		it('should return null if input is null', function() {
			//Arrange
			let stringToDecrypt = null,
				expected = null,
				actual = undefined;
			
			//Act
			actual = CipherHelper.decrypt(stringToDecrypt);
			
			//Assert
			Assert.areEqual(expected, actual);
		});
		
		it('should return empty string if input is empty string', function() {
			//Arrange
			let stringToDecrypt = '',
				expected = '',
				actual = undefined;
			
			//Act
			actual = CipherHelper.decrypt(stringToDecrypt);
			
			//Assert
			Assert.areEqual(expected, actual);
		});
		
		it('should return null for invalid input', function() {
			//Arrange
			let stringToDecrypt = 'Unencrypted String',
				expected = null,
				actual = undefined;
			
			//Act
			actual = CipherHelper.decrypt(stringToDecrypt);
			
			//Assert
			Assert.areEqual(expected, actual);
		});
	});
	
	describe('misc', function() {
		before(function () {
			Logger.debug('before-misc called');
		});
		
		after(function () {
			Logger.debug('after-misc called');
		});
		
		beforeEach(function () {
			Logger.debug('beforeEach-misc called');
		});
		
		afterEach(function () {
			Logger.debug('afterEach-misc called');
		});
		
		it('should throw an error, and it does', function() {
			//Arrange
			//Act
			CipherHelper.throwException();
			//Assert
		}).expectedError('demo exception');
		
		it('should throw an error, but it does not', function() {
			//Arrange
			//Act
			Logger.debug('This is an example of a failed test.');
			CipherHelper.doesNotThrowException();
			//Assert
		}).expectedError('demo exception');
		
		it('test using chai assertion library', function () {
			//Arrange
			var expect = require('/app_test/cartridge/scripts/lib/chai').expect
			  , foo = 'bar'
			  , beverages = { tea: [ 'chai', 'matcha', 'oolong' ] };
			
			//Act
			
			
			//Assert
			expect(foo).to.be.a('string');
			expect(foo).to.equal('bar');
			expect(foo).to.have.length(3);
			expect(beverages).to.have.property('tea').with.length(3);
		});
	});
	
});